package myzee.beans;

public class HelloWorld {
	private String message;
	
	public HelloWorld() {
		System.out.println("bean instance created");
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
