package myzee.driver;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import myzee.beans.HelloWorld;

public class TestHelloWorld {

	public static void main(String[] args) {
		
		/*
		 * Using scope = "singleton", It creates the bean instances at the time of loading xml
		 */
		System.out.println("Using scope = \"singleton\"");

		ApplicationContext context = new ClassPathXmlApplicationContext("myzee/resources/spring_singleton.xml");
//		HelloWorld h = (HelloWorld) context.getBean("hw");
		
		
		/*
		 * Using scope = "prototype", It creates the bean instances at the time of request.
		 * Here in below example, it creates three instances.
		 */
		ApplicationContext context_prototype = new ClassPathXmlApplicationContext("myzee/resources/spring_prototype.xml");
		System.out.println("\n\nUsing scope = \"prototype\"\n");
		HelloWorld h = (HelloWorld) context_prototype.getBean("hw");
		HelloWorld h1 = (HelloWorld) context_prototype.getBean("hw");
		HelloWorld h2 = (HelloWorld) context_prototype.getBean("hw");
		/*
		 * Note : if the scope = "prototype, then ApplicationContext acts as BeanFactory"
		 */
	}
}
